# Example test
A minimal example test.

## How to run it
Please refer to the top-level README.md for common dependencies. There are no
test-specific dependencies.

### Install dependencies
```bash
root# bash ../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
